Vagrant scripts to bring up two VMs to test Pacemaker (https://clusterlabs.org/pacemaker/) and keepalived (https://www.keepalived.org/) as a failover solution for a Apache setup.

Requirements: Ansible >= 2.9.10, Vagrant >= 2.2.9, VirtualBox >= 6.1

How to use this:

* Clone the repo
* Go to the directory where the `Vagrantfile` is and run `vagrant up`
* Go to http://192.168.33.13:8080/
* To test the high availability, stop the machines or services. 

